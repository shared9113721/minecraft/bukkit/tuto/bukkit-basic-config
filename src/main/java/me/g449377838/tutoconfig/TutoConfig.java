package me.g449377838.tutoconfig;

import org.bukkit.plugin.java.JavaPlugin;

import me.g449377838.tutoconfig.managers.ApiRequestManager;
import me.g449377838.tutoconfig.managers.ConfigManager;

public final class TutoConfig extends JavaPlugin {
	private static ConfigManager configManager;

	@Override
	public void onEnable() {
		// Je configure la config
		setupConfig();

		// Ici, juste une démo pour te montrer comment utiliser la config.
		// P.S. : Ça log simplement l'URL ^^
		ApiRequestManager.getApiConnection();
	}

	private void setupConfig() {
		// J'utilise une classe pour gérer la logique, etc., ce sera plus propre :O
		configManager = new ConfigManager(this);
	}

	public static ConfigManager getConfigManager() {
		// Ici, je crée une méthode pour renvoyer spécifiquement la config.
		// Comme `ConfigManager` est directement construit avec `this`, je n'ai pas besoin d'exposer l'instance.
		return configManager;
	}
}
