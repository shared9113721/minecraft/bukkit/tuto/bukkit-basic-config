package me.g449377838.tutoconfig.managers;

import me.g449377838.tutoconfig.TutoConfig;

public final class ApiRequestManager {
	private ApiRequestManager() {}

	public static void getApiConnection() {
		String url = TutoConfig.getConfigManager().getApiUrl();

		// Ici, il faudrait utiliser un LOGGER plutôt qu'un `System.out`
		System.out.println(String.format("GET %s.", url));
	}
}
