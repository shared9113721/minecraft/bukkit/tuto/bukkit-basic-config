package me.g449377838.tutoconfig.managers;

import me.g449377838.tutoconfig.TutoConfig;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.IOException;

public class ConfigManager {
	private final TutoConfig plugin;

	public ConfigManager(TutoConfig plugin) {
		this.plugin = plugin;

		// Ici, je fais un `saveDefaultConfig()`, cela va copier ma config si elle n'existe pas.
		// Cela ne fera rien s'il y a déjà un fichier `config.yml`, même vide.
		// Si des `path` n'existent pas dans la config sur le serveur, il ne va pas les créer. Ce sera à toi de gérer les erreurs.

		// Attention :
		// Donc si tu changes la config dans ressources, et que tu es en dev. pensent à suprimmer la config créer sur ton serveur :D
		plugin.saveDefaultConfig();
	}

	public void reloadConfig() throws IOException {
		// Ici, il suffit d'appeler : `reloadConfig()` pour recharger le plugin.
		// Par exemple via une commande (je n'ai pas implémenté la commande pour ne pas surcharger le tuto) :d

		// Attention : ici cela peut échouer si la config a disparu ou si le jar n'a plus les droits, etc., il faut le gérer et recréer la config avec `saveDefaultConfig()`.
		// Mais comme c'est juste un Plugin Test, je ne me suis pas embêté à le faire :p
		plugin.reloadConfig();
	}

	public String getApiUrl() {
		// Ici, au lieu d'exposer le `FileConfiguration`, je préprogramme les méthodes :
		// - 1 : Comme ça, toutes les configs sont au même endroit et si on change le `path` d'une configuration, on a qu'un fichier à modifier.
		// - 2 : Je pourrais gérer la logique avant de donner les données, donc on sait que celui qui va appeler ça n'a pas besoin de gérer les erreurs et tout se trouve ici.
		FileConfiguration config = plugin.getConfig();

		// Dans ce cas, je n'ai pas fait de gestion d'erreur ^^.
		return config.getString("api");
	}
}
