# TutoConfig Plugin

Ce document résume comment créer et gérer une configuration dans un plugin Minecraft Bukkit/Spigot.

**Vous trouverez plus de détails importants dans les classes (fichier)**, ainsi que des explications sur les raisons de certaines approches et des erreurs à éviter.

*Ce tutoriel est basé sur la version 1.20.4. Pour les versions antérieures ou ultérieures, certaines choses pourraient légèrement changer.*

## Création d'une Configuration

### 1. Création de la Config

Créez un fichier `config.yml` dans le dossier `/resources` à la racine de votre projet :

```yaml
# Ma jolie config :D
api: "http://example.com/api"
```

### 2. Initialisation de la Configuration

Dans la classe principale du plugin (TutoConfig.java), initialisez la configuration lors de l'activation du plugin :

```java
public final class TutoConfig extends JavaPlugin {
	private static ConfigManager configManager;

	@Override
	public void onEnable() {
		// Configure la configuration
		setupConfig();

		// Code ...
	}

	private void setupConfig() {
		// Utilisation d'une classe pour gérer la logique de configuration
		configManager = new ConfigManager(this);
	}

	public static ConfigManager getConfigManager() {
		// Un getter pour rendre la config disponible
		return configManager;
	}
}
```

### 3. Gestion de la Configuration

Utilisez une classe `ConfigManager` pour gérer la configuration :

```java
public class ConfigManager {
	private final TutoConfig plugin;

	public ConfigManager(TutoConfig plugin) {
		this.plugin = plugin;

		// Sauvegarde la configuration par défaut si elle n'existe pas
		plugin.saveDefaultConfig();
	}

	public void reloadConfig() throws IOException {
		// Recharge la configuration
		plugin.reloadConfig();
	}
}
```

Ajoutez une méthode dans `ConfigManager` pour exposer une partie de la config:

```java
public String getApiUrl() {
	// Récupère l'URL de l'API depuis la configuration
	FileConfiguration config = plugin.getConfig();
	return config.getString("api");
}
```

### 4. Example d'utilisation de la Configuration

Utilisez la configuration dans une autre classe, par exemple `ApiRequestManager` :

```java
public class ApiRequestManager {
	public static void getApiConnection() {
		String url = TutoConfig.getConfigManager().getApiUrl();

		// Code ...
	}
}
```

## Notes

J'ai rédigé ce tutoriel rapidement. Les noms des packages et des classes ne sont pas obligatoires et certain truc peuvent être amélioré, par exemple: la logique de gestion des erreurs et/ou la recuperation de la donné pourrait être séparée dans une autre classe.
